package it.polimi.patterns.architectural.mvc.view;

import it.polimi.patterns.behavioral.observer.StudentObserver;
import it.polimi.patterns.common.Student;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainWindow extends JFrame implements ActionListener, Observer{

	private StudentObserver so;
	private Map<String, JComponent> controls;
	public MainWindow(){
		controls = new HashMap<String, JComponent>();
		initializeComponents();
	}
	
	private void initializeComponents(){
		setLocation(100, 100);
		setSize(800, 600);
		setTitle("Student");
		JPanel panel = new JPanel(new GridLayout(6, 2));
		panel.add("idLabel", new JLabel("Id:"));
		JLabel toAdd = new JLabel();
		controls.put("idValue", toAdd);
		panel.add(toAdd);		
		panel.add("nameLabel", new JLabel("Name:"));
		
		JTextField tfToAdd = new JTextField();  
		controls.put("nameValue", new JTextField());
		panel.add(tfToAdd);
		panel.add("surnameLabel", new JLabel("Surname:"));
		
		tfToAdd = new JTextField(); 
		controls.put("surnameValue", tfToAdd);
		panel.add(tfToAdd);
		panel.add("birthLabel", new JLabel("Birthdate:"));
		
		tfToAdd = new JTextField(); 
		controls.put("birthValue", tfToAdd);
		panel.add(tfToAdd);
		panel.add("ageLabel", new JLabel("Age:"));
		toAdd = new JLabel();
		controls.put("ageValue", toAdd);
		panel.add(toAdd);
		panel.add("", new JLabel());
		JButton saveBtn = new JButton("Save");
		saveBtn.addActionListener(this);		
		panel.add(saveBtn);
		add(panel);
		setVisible(true);
	}

	public StudentObserver getSo() {
		return so;
	}

	public void setSo(StudentObserver so) {
		this.so = so;
	}

	public void actionPerformed(ActionEvent e) {
		Student s = new Student();
		s.setId(((JLabel)(controls.get("idValue"))).getText());
		s.setName(((JTextField)(controls.get("nameValue"))).getText());
		s.setSurname(((JTextField)(controls.get("surnameValue"))).getText());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			s.setBirthdate(new Date(sdf.parse(((JTextField)(controls.get("birthValue"))).getText()).getTime()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		so.saveStudent(s);
		
	}

	public void update(Observable arg0, Object arg1) {
		Student s = (Student)arg0;
		if(arg1.equals("id")){
			((JLabel)(controls.get("idValue"))).setText(s.getId());
		} else if (arg1.equals("name")){
			((JTextField)(controls.get("nameValue"))).setText(s.getName());
		} else if (arg1.equals("surname")){
			((JTextField)(controls.get("surnameValue"))).setText(s.getSurname());
		}  else if (arg1.equals("birthdate")){
			((JTextField)(controls.get("birthValue"))).setText(s.getBirthdate().toString());
		} else {
			((JLabel)(controls.get("ageValue"))).setText(s.getAge()+"");
		}
		
	}
	
	
}
