package it.polimi.patterns.behavioral.state;

public class StartState implements GameState{

	public void Handle() {
		System.out.println("Benvenuto nel tuo registro elettronico");
		System.out.println("Cosa vuoi fare?");
		System.out.println("1) Inserisci uno studente");
		System.out.println("2) Modifica uno studente");
		System.out.println("3) Elimina uno studente");
		System.out.println("4) Esci");
		//...
		
	}

}
