package it.polimi.patterns.behavioral.state;

public interface GameState {
	void Handle();
}
