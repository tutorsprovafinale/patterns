package it.polimi.patterns.behavioral.observer;

import it.polimi.patterns.architectural.mvc.view.MainWindow;
import it.polimi.patterns.common.Student;
import it.polimi.patterns.creational.abstractfactory.DAOCreatorFactory;
import it.polimi.patterns.creational.abstractfactory.InMemoryDAOCreatorFactory;
import it.polimi.patterns.creational.factorymethod.StudentDAO;

import java.util.Observable;
import java.util.Observer;

public class StudentObserver implements Observer{

	private MainWindow mw = null;
	
	public StudentObserver(){
		
	}
	
	
	
	public MainWindow getMw() {
		return mw;
	}

	public void setMw(MainWindow mw) {
		this.mw = mw;
	}



	public void saveStudent(Student s){
		DAOCreatorFactory fac = new InMemoryDAOCreatorFactory();
    	StudentDAO dao = fac.getDaoCreator().getStudentDAO();
    	dao.updateStudent(s);
	}
	
	public void update(Observable o, Object arg) {
		//mw.update(o, arg);
		
	}

}
