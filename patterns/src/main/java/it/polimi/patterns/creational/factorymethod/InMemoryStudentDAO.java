package it.polimi.patterns.creational.factorymethod;

import it.polimi.patterns.common.Student;
import it.polimi.patterns.creational.singleton.InMemoryDB;

public class InMemoryStudentDAO implements StudentDAO {

	private InMemoryDB db;
	
	public InMemoryStudentDAO() {
		db = InMemoryDB.getInstance();
	}
	
	public String insertStudent(Student s) {
		db.getStudentTable().put(s.getId(), s);
		return s.getId();
	}

	public void deleteStudent(String id) {
		db.getStudentTable().remove(id);
	}

	public void updateStudent(Student s) {
		Student old = db.getStudentTable().get(s.getId());
		old.setName(s.getName());
		old.setSurname(s.getSurname());
		old.setBirthdate(s.getBirthdate());
	}

	public Student getStudent(String id) {
		return db.getStudentTable().get(id);
	}

}
