package it.polimi.patterns.creational.factorymethod;

import it.polimi.patterns.common.Student;

public interface StudentDAO {
	String insertStudent(Student s);
	void deleteStudent(String id);
	void updateStudent(Student s);
	Student getStudent(String id);
}
