package it.polimi.patterns.creational.abstractfactory;

import it.polimi.patterns.creational.factorymethod.DAOCreator;

public interface DAOCreatorFactory {
	DAOCreator getDaoCreator();
}
