package it.polimi.patterns.creational.factorymethod;

public class InMemoryCreator implements DAOCreator {

	public StudentDAO getStudentDAO() {
		return new InMemoryStudentDAO();
	}
	
}
