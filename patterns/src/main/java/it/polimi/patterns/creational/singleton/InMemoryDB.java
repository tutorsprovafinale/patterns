package it.polimi.patterns.creational.singleton;

import it.polimi.patterns.common.Student;

import java.util.HashMap;
import java.util.Map;

public class InMemoryDB {

	private static InMemoryDB instance;
	
	private Map<String, Student> studentTable;
	
	private InMemoryDB(){
		studentTable = new HashMap<String, Student>();
	}
	
	public static InMemoryDB getInstance(){
		if(instance == null) instance=new InMemoryDB();
		return instance;
	}
	
	public Map<String, Student> getStudentTable(){
		return studentTable;
	}
}
