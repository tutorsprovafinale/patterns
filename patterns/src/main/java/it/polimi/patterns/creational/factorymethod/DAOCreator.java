package it.polimi.patterns.creational.factorymethod;

public interface DAOCreator {
	StudentDAO getStudentDAO();
}
