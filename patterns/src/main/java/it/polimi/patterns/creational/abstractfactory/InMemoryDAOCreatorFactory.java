package it.polimi.patterns.creational.abstractfactory;

import it.polimi.patterns.creational.factorymethod.DAOCreator;
import it.polimi.patterns.creational.factorymethod.InMemoryCreator;

public class InMemoryDAOCreatorFactory implements DAOCreatorFactory{

	public InMemoryDAOCreatorFactory() {
	}
	
	public DAOCreator getDaoCreator() {
		return new InMemoryCreator();
	}

}
