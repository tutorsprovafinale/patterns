package it.polimi.patterns.structural;

import java.util.UUID;

import it.polimi.patterns.common.Student;

public class AutoStudentDAO extends IAutoStudentDAO {

	
	
	public AutoStudentDAO() {
		super();
	}

	public String insertStudent(Student s) {
		s.setId(UUID.randomUUID().toString());
		return dao.insertStudent(s);
	}

	public void deleteStudent(String id) {
		dao.deleteStudent(id);

	}

	public void updateStudent(Student s) {
		dao.updateStudent(s);

	}

	public Student getStudent(String id) {
		
		return dao.getStudent(id);
	}

}
