package it.polimi.patterns;

import java.sql.Date;
import java.util.UUID;

import it.polimi.patterns.architectural.mvc.view.MainWindow;
import it.polimi.patterns.behavioral.observer.StudentObserver;
import it.polimi.patterns.common.Student;
import it.polimi.patterns.creational.abstractfactory.DAOCreatorFactory;
import it.polimi.patterns.creational.abstractfactory.InMemoryDAOCreatorFactory;
import it.polimi.patterns.creational.factorymethod.StudentDAO;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
    	DAOCreatorFactory fac = new InMemoryDAOCreatorFactory();
    	StudentDAO dao = fac.getDaoCreator().getStudentDAO();
    	Student s = new Student();
    	StudentObserver so = new StudentObserver();
    	s.addObserver(so);
    	MainWindow mw = new MainWindow();
    	Thread.sleep(1000);
    	mw.setSo(so);
    	s.addObserver(mw);
    	s.setId(UUID.randomUUID().toString());
    	s.setName("Riccardo");
    	s.setSurname("Cipolleschi");
    	s.setBirthdate(new Date(System.currentTimeMillis()-1000*60*60*24*365*28));
    	dao.insertStudent(s);
    	
    	
    	
    	
        
    }
}
