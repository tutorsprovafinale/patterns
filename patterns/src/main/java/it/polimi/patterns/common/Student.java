package it.polimi.patterns.common;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Student extends Observable{
	private String id;
	private String name;
	private String surname;
	private java.sql.Date birthdate;
	private List<Observer> observers;
	
	public Student(String id, String name, String surname, Date birthdate) {
		super();
		this.observers = new ArrayList<Observer>();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.birthdate = birthdate;
	}
	public Student() {
		super();
		this.observers = new ArrayList<Observer>();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
		setChanged();
		notifyObservers("id");
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		setChanged();
		notifyObservers("name");
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
		setChanged();
		notifyObservers("surname");
	}
	public java.sql.Date getBirthdate() {
		return birthdate;
	}
	
	public int getAge(){
		Date now = new Date(System.currentTimeMillis());
		int diff = now.getYear() - birthdate.getYear();
		if(now.getMonth() > birthdate.getMonth()){
			diff++;
		} else if(now.getMonth() == birthdate.getMonth()) {
			if(now.getDate() >= birthdate.getDate()) diff++;
		}
		return diff;
	}
	
	public void setBirthdate(java.sql.Date birthdate) {
		this.birthdate = birthdate;
		setChanged();
		notifyObservers("birthdate");
		setChanged();
		notifyObservers("age");
	}
	
	/*@Override
	public synchronized void notifyObservers(Object prop) {
		for(Observer o : observers){
			o.update(this, prop);
		}		
	}
	@Override
	public synchronized void addObserver(Observer o) {
		observers.add(o);
		super.addObserver(o);
	}*/
	
	
	
}
